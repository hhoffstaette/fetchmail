#include "fetchmail.h"
#include <regex.h>

int match_regex(const char *restrict regexpattern, const char *restrict ins)
/** match_regex matches \a ins case-insensitively against the extended POSIX regular expression \a regexpattern.
 * \return 1 for a match, 0 for no match, -1 for an error it has already report()ed.
 */
{
	regex_t re;
	int rcerr = regcomp(&re, regexpattern, REG_EXTENDED | REG_ICASE | REG_NOSUB);
	if (rcerr) {
		size_t errsiz = regerror(rcerr, &re, NULL, 0);
		char *errbuf = (char *)(errsiz ? xmalloc(errsiz) : NULL);
		(void)regerror(rcerr, &re, errbuf, errsiz);

		report(stderr, GT_("Internal error: match_regex could not compile pattern %s: %s\n"), regexpattern, errbuf ? errbuf : GT_("(unknown regcomp error)"));
		xfree(errbuf);
		return -1;
	}
	int match = regexec(&re, ins, 0, NULL, 0); // 0 for match, REG_NOMATCH for not matched
	regfree(&re);
	return !match;
}
